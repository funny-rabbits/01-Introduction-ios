// MARK: Задание 1:
// Составить сводную информацию о себе и вывести ее в консоль.

let fullName = "Шарипова Аделина Альмировна"
let age = 21
let materialStatus = "Не замужем"
let children = "Нет"
let education = "СПбГЭТУ ЛЭТИ"
let job = "ООО Астра инженерные системы"

print("ФИО: ", fullName)
print("Возраст: ", age)
print("Семейное положение: ", materialStatus)
print("Дети: ", children)
print("Специальность: ", education)
print("Работа: ", job)
